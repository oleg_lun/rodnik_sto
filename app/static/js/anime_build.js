$(document).ready(function() {
	
/*  var lineDrawing = anime({
  targets: '#lineDrawing .lines path',
  strokeDashoffset: [anime.setDashoffset, 0],
  easing: 'easeInOutSine',
  duration: 2000,
  delay: function(el, i) { return i * 250 },
  direction: 'alternate',
  loop: false
  });*/

// question code
  var translate = anime({
    targets: '#question',
    top: [
      { value: '430px', duration: 0, delay: 1000},
      { value: '110px', duration: 300, delay: 2000}
    ],
    easing: 'linear'
  });

// 'but' code
var but = anime({
  targets: '#but',
  opacity: [
    { value: 0, duration: 1000, delay: 0},
    { value: 1, duration: 1000, delay: 2000},
    { value: 0, duration: 1000, delay: 4750}
  ],
  letterSpacing: [
    { value: '-0.5em', duration: 1000, delay: 0},
    { value: '0', duration: 1000, delay: 2000},
    { value: '1em', duration: 1000, delay: 4750}
  ],
  transform: [
    { value: 'translateZ(-700px)', duration: 1000, delay: 2000},
    { value: 'translateZ(0)', duration: 1000, delay: 500}
  ],
  filter: [
    { value: 'none', duration: 1000, delay: 2000},
    { value: 'blur(12px)', duration: 1000, delay: 5500}
  ],
  loop: false
});

// falling blocks
  var fall = anime({
  targets: '#money',
  /*top: '260px',
  left: '40px',*/
  top: [
    {value: '260px', duration: 500, delay: 4000},
    {value: '200px', duration: 2000, delay: 4200}
  ],
  left: [
    {value: '40px', duration: 500, delay: 4000},
    {value: '-700px', duration: 2000, delay: 4200}
  ],
  rotate: [
    {value: '10turn', duration: 1000, delay: 8700, easing: 'linear'}
  ],
  easing: 'easeOutElastic'
/*  offset: 4000,
  duration: 500*/
  });

  var fall = anime({
  targets: '#time',
  /*top: '260px',
  right: '40px',*/
  top: [
    {value: '260px', duration: 500, delay: 5000},
    {value: '200px', duration: 2000, delay: 3200}
  ],
  right: [
    {value: '40px', duration: 500, delay: 5000},
    {value: '-700px', duration: 2000, delay: 3200}
  ],
  rotate: [
    {value: '10turn', duration: 1000, delay: 8700, easing: 'linear'}
  ],
  easing: 'easeOutElastic',
/*  offset: 5000,
  duration: 500*/
  });

  var fall = anime({
  targets: '#team',
/*  top: '600px',
  left: '638px',*/
  top: [
    {value: '600px', duration: 500, delay: 6000},
    {value: '2000px', duration: 2000, delay: 2200}
  ],
  left: [
    {value: '638px', duration: 500, delay: 6000},
    {value: '700px', duration: 2000, delay: 2200}
  ],
  rotate: [
    {value: '10turn', duration: 1000, delay: 8700, easing: 'linear'}
  ],
  easing: 'easeOutElastic',
/*  offset: 6000,
  duration: 500*/
  });

// ufr destroys
  var fall = anime({
  targets: '#logo',
  top: '280px',
  left: '752px',
  easing: 'easeOutElastic',
  offset: 8500
  });

  /*var fall = anime({
  targets: '#money',
  top: '240px',
  left: '-900px',
  opacity: 0,
  rotate: '10turn',
  easing: 'easeInOutQuad',
  offset: 8500
  });
  var fall = anime({
    targets: '#time',
    
    opacity: 0,
    rotate: '10turn',
    easing: 'easeInOutQuad',
    offset: 8500
  });
  var fall = anime({
    targets: '#team',
    
    opacity: 0,
    rotate: '10turn',
    easing: 'easeInOutQuad',
    offset: 8500
  });*/

// here comes ufr
  var cssProperties = anime({
  targets: '#ufr',
  opacity: 1,
  easing: 'easeInBack',
  offset: 9000,
  duration: 2000
  });

// just do it
  var cssProperties = anime({
  targets: '#start',
  opacity: 1,
  scale: 1,
  easing: 'easeInOutQuart',
  offset: '11000',
  duration: 3000
  });
  
});
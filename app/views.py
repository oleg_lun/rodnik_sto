# -*- coding: utf-8 -*-
from app import app
from flask import  render_template, request
from urllib import urlencode
import requests
import json

@app.route('/',methods=['GET'])
def home():
    return render_template("index.html")

